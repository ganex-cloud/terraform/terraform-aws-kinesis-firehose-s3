module "kinesis-firehose_test" {
  source                     = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-kinesis-firehose-s3.git?ref=master"
  stream_name                = "test"
  s3_bucket_name             = module.s3_test.bucket_name
  s3_bucket_arn              = module.s3_test.bucket_arn
  s3_compression_format      = "GZIP"
  s3_buffer_interval         = 300
  s3_buffer_size             = 5
  cloudwatch_logging_enabled = true
}

module "s3_test" {
  source                  = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-s3-bucket.git?ref=0.12"
  name                    = "test"
  acl                     = "private"
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm = "AES256"
      }
    }
  }
}
