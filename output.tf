output "kinesis_firehose_stream_role_arn" {
  value       = join(",", aws_iam_role.kinesis_firehose_stream_role.*.arn)
  description = "The ARN of the IAM role to allow access to Elasticsearch cluster"
}
