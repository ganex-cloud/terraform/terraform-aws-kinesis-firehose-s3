variable "stream_name" {
  description = "Name to be use on kinesis firehose stream"
  type        = string
}

variable "stream_server_side_encryption" {
  description = "(Optional) Encrypt at rest options. Server-side encryption should not be enabled when a kinesis stream is configured as the source of the firehose delivery stream."
  type        = bool
  default     = false
}

variable "s3_bucket_name" {
  description = "(Required) The S3 bucket name"
  type        = string
}

variable "s3_bucket_arn" {
  description = "(Required) The ARN of the S3 bucket"
  type        = string
}

variable "s3_buffer_size" {
  description = "(Optional) Buffer incoming data to the specified size, in MBs, before delivering it to the destination."
  type        = number
  default     = 5
}

variable "s3_buffer_interval" {
  description = "(Optional) Buffer incoming data for the specified period of time, in seconds, before delivering it to the destination. "
  type        = number
  default     = 300
}

variable "s3_compression_format" {
  description = "(Optional) The compression format. If no value is specified, the default is UNCOMPRESSED."
  type        = string
  default     = "UNCOMPRESSED"
}

variable "s3_kms_key_arn" {
  description = "(Optional) Specifies the KMS key ARN the stream will use to encrypt data. If not set, no encryption will be used."
  type        = string
  default     = ""
}

variable "s3_error_output_prefix" {
  type        = string
  description = "Prefix added to failed records before writing them to S3. This prefix appears immediately following the bucket name."
  default     = ""
}

variable "s3_prefix" {
  type        = string
  description = "An extra S3 Key prefix prepended before the time format prefix of records delivered to the AWS S3 Bucket."
  default     = ""
}

variable "cloudwatch_logging_enabled" {
  description = "Enable cloudwatch logs"
  type        = bool
  default     = false
}
