data "aws_caller_identity" "default" {}
data "aws_region" "default" {}

locals {
  s3_kms_key_arn = "${var.s3_kms_key_arn == "" ? "arn:aws:kms:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:key/NO-VALUE" : var.s3_kms_key_arn}"
}

resource "aws_kinesis_firehose_delivery_stream" "this" {
  name        = var.stream_name
  destination = "extended_s3"

  server_side_encryption {
    enabled = var.stream_server_side_encryption
  }

  extended_s3_configuration {
    bucket_arn          = var.s3_bucket_arn
    buffer_size         = var.s3_buffer_size
    buffer_interval     = var.s3_buffer_interval
    compression_format  = var.s3_compression_format
    error_output_prefix = var.s3_error_output_prefix
    role_arn            = aws_iam_role.kinesis_firehose_stream_role.arn
    prefix              = var.s3_prefix
    kms_key_arn         = length(var.s3_kms_key_arn) > 0 ? var.s3_kms_key_arn : null
    s3_backup_mode      = "Disabled"

    cloudwatch_logging_options {
      enabled         = true
      log_group_name  = aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name
      log_stream_name = aws_cloudwatch_log_stream.kinesis_firehose_stream_logging_s3_stream.name
    }

  }
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "kinesis_firehose_stream_policy" {
  statement {
    effect = "Allow"
    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject"
    ]

    resources = [
      "${var.s3_bucket_arn}",
      "${var.s3_bucket_arn}/*",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "kms:Decrypt",
      "kms:GenerateDataKey"
    ]

    resources = [
      "${local.s3_kms_key_arn}"
    ]

    condition {
      test     = "StringEquals"
      variable = "kms:ViaService"
      values   = ["s3.${data.aws_region.default.name}.amazonaws.com"]
    }
  }

  statement {
    effect = "Allow"
    actions = [
      "kinesis:DescribeStream",
      "kinesis:GetShardIterator",
      "kinesis:GetRecords",
      "kinesis:ListShards"
    ]

    resources = [
      "arn:aws:kinesis:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:stream/${var.stream_name}"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:log-group:${aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name}:log-stream:*",
    ]
  }
}

resource "aws_iam_role" "kinesis_firehose_stream_role" {
  name               = "${var.stream_name}-kinesis_firehose_stream_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_role_policy" "kinesis_firehose_policy" {
  name   = "kinesis_firehose_policy"
  role   = aws_iam_role.kinesis_firehose_stream_role.name
  policy = data.aws_iam_policy_document.kinesis_firehose_stream_policy.json
}

resource "aws_cloudwatch_log_group" "kinesis_firehose_stream_logging_group" {
  name = "/aws/kinesisfirehose/${var.stream_name}"
}

resource "aws_cloudwatch_log_stream" "kinesis_firehose_stream_logging_s3_stream" {
  log_group_name = aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name
  name           = "S3Delivery"
}
